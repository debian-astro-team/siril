Source: siril
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Vincent Hourdin <debian-siril@free-astro.vinvin.tf>, Cyril Richard <lock042@gmail.com>
Section: science
Priority: optional
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian-astro-team/siril.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/siril
Homepage: https://www.siril.org
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               intltool,
               meson (>= 0.51),
               git,
               cmake,
               libcfitsio-dev,
               libexiv2-dev,
               libffms2-dev,
               libfftw3-dev,
               libgsl-dev,
               libgtk-3-dev,
               libjson-glib-dev,
               libopencv-dev,
               libpng-dev,
               libraw-dev,
               libtiff-dev,
               libavformat-dev,
               libavutil-dev,
               libavcodec-dev,
               libswscale-dev,
               libswresample-dev,
               libheif-dev,
               wcslib-dev

Package: siril
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         siril-common (= ${source:Version}),
         librsvg2-2
Recommends: gvfs-backends
Suggests: gnuplot
Description: astronomical image processing tool
 Siril is an image processing tool specially tailored for noise reduction and
 improving the signal/noise ratio of an image from multiple captures, as
 required in astronomy. Siril can align automatically or manually, stack and
 enhance pictures from various file formats, even images sequences (movies and
 SER files).

Package: siril-common
Architecture: all
Depends: ${misc:Depends}
Replaces: siril (<< 0.9.12)
Breaks: siril (<< 0.9.12)
Multi-Arch: foreign
Description: architecture-independent files for siril
 Siril is an image processing tool specially tailored for noise reduction and
 improving the signal/noise ratio of an image from multiple captures, as
 required in astronomy. Siril can align automatically or manually, stack and
 enhance pictures from various file formats, even images sequences (movies and
 SER files).
 .
 This package provides the architecture-independent files (Siril scripts
 and other files).
